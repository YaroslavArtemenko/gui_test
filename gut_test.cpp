// g++ test.cpp `pkg-config --libs gtk+-3.0` `pkg-config --cflags gtk+-3.0`


#include <gtk/gtk.h>

//scale image in container
gboolean draw_picture(GtkWidget *da, cairo_t *cr, gpointer data)
{
  gint width=gtk_widget_get_allocated_width(da);
  gint height=gtk_widget_get_allocated_height(da);

  GdkPixbuf *temp=gdk_pixbuf_scale_simple((GdkPixbuf*)data, width, height, GDK_INTERP_BILINEAR);
  gdk_cairo_set_source_pixbuf(cr, temp, 0, 0);
  cairo_paint(cr);

  g_object_unref(temp);
  return FALSE;
}

GtkWidget *window1;

//clean container with image for update
void clear_container() {
    GList *children, *iter;
    children = gtk_container_get_children(GTK_CONTAINER(window1));
    for(iter = children; iter != NULL; iter = g_list_next(iter))
        gtk_widget_destroy(GTK_WIDGET(iter->data));
    g_list_free(children);
}

static void button_clicked_red(GtkWidget* widget)
{
    clear_container();

    g_print("Button 1 is pressed\n");

    GdkPixbuf *pixbuf=gdk_pixbuf_new_from_file("/home/pi/Documents/OneDrive_1_26.7.2021/red.png", NULL);

    GtkWidget *da1=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da1, TRUE);
    gtk_widget_set_vexpand(da1, TRUE);
    g_signal_connect(da1, "draw", G_CALLBACK(draw_picture), pixbuf);

    GtkWidget *grid=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), da1, 0, 0, 1, 1);

    gtk_container_add(GTK_CONTAINER(window1), grid);
   
    gtk_widget_show_all(window1);
}

static void button_clicked_orange(GtkWidget* widget)
{
    clear_container();

    g_print("Button 2 is pressed\n");

    GdkPixbuf *pixbuf=gdk_pixbuf_new_from_file("/home/pi/Documents/OneDrive_1_26.7.2021/orange.png", NULL);

    GtkWidget *da1=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da1, TRUE);
    gtk_widget_set_vexpand(da1, TRUE);
    g_signal_connect(da1, "draw", G_CALLBACK(draw_picture), pixbuf);

    GtkWidget *grid=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), da1, 0, 0, 1, 1);

    gtk_container_add(GTK_CONTAINER(window1), grid);
   
    gtk_widget_show_all(window1);
}

static void button_clicked_green(GtkWidget* widget)
{
    clear_container();

    g_print("Button 3 is pressed\n");

    GdkPixbuf *pixbuf=gdk_pixbuf_new_from_file("/home/pi/Documents/OneDrive_1_26.7.2021/green.png", NULL);

    GtkWidget *da1=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da1, TRUE);
    gtk_widget_set_vexpand(da1, TRUE);
    g_signal_connect(da1, "draw", G_CALLBACK(draw_picture), pixbuf);

    GtkWidget *grid=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), da1, 0, 0, 1, 1);

    gtk_container_add(GTK_CONTAINER(window1), grid);
   
    gtk_widget_show_all(window1);
}

//create window for images
void create_window_for_image() {
    window1 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(window1), 600, 600);
    gtk_window_set_position(GTK_WINDOW(window1), GTK_WIN_POS_CENTER);
}

//create labels and buttons for first Window
GtkWidget *label1, *label2, *label3, *button1, *button2, *button3;
void create_labels_buttons() {

    button1 = gtk_button_new_with_label("red");
    button2 = gtk_button_new_with_label("orange");
    button3 = gtk_button_new_with_label("green");

    g_signal_connect(button1, "clicked", G_CALLBACK(button_clicked_red), NULL);
    g_signal_connect(button2, "clicked", G_CALLBACK(button_clicked_orange), NULL);
    g_signal_connect(button3, "clicked", G_CALLBACK(button_clicked_green), NULL);
}
 
 
int main(int argc, char* argv[])
{
    gtk_init(&argc, &argv);
    
    GtkWidget *window, *vbox;
    
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    g_signal_connect(window, "delete-event", G_CALLBACK(gtk_main_quit), NULL);

    //result 
    create_window_for_image();

    //first window with
    create_labels_buttons();

    vbox = gtk_vbox_new(0, 0);
    gtk_box_pack_start(GTK_BOX(vbox), button1, 1, 1, 0);
    gtk_box_pack_start(GTK_BOX(vbox), button2, 1, 1, 0);
    gtk_box_pack_start(GTK_BOX(vbox), button3, 1, 1, 0);
    
    gtk_container_add(GTK_CONTAINER(window), vbox);
    
    gtk_widget_show_all(window);
    gtk_main();
    return 0;
}
